static int playbackTick = -1;
static int botIndex = -1;

void OnPluginEnd_Replays()
{
	if (botIndex && IsValidEntity(botIndex))
	{
		AcceptEntityInput(botIndex, "kill");
	}
}

Action OnPlayerSpawned_Replays(int client)
{
	if (botIndex != client)
	{
		return Plugin_Continue;
	}
	
	SetClientName(botIndex, DEFAULT_BOT_NAME);
	return Plugin_Handled;
}

void OnPlayerRunCmdPost_Replays(int client)
{
	if (!GCIsValidClient(client) || !IsFakeClient(client) || client != botIndex)
	{
		return;
	}
	
	// Bot playback
	if (playbackTick < 0 || g_alReplayData == null)
	{
		return;
	}
	
	if (playbackTick >= g_alReplayData.Length)
	{
		SetEntityMoveType(client, MOVETYPE_NONE);
		CreateTimer(2.0, Timer_StopPlayback, INVALID_HANDLE, TIMER_FLAG_NO_MAPCHANGE);
		return;
	}
	
	ReplayTick replayTick;
	g_alReplayData.GetArray(playbackTick, replayTick);
	
	// HACK: shitty interpolation thing.
	float newYaw;
	{
		int tick = GCIntMax(playbackTick - 1, 0);
		int tick2 = GCIntMax(playbackTick - 2, 0);
		int tick3 = GCIntMax(playbackTick - 3, 0);
		
		float lastYaw = g_alReplayData.Get(tick, BLOCK_ANGLES + 1);
		float avg1 = replayTick.angles[1] - (GCNormaliseYaw(replayTick.angles[1] - lastYaw) * 0.5);
		
		float lastLastYaw = g_alReplayData.Get(tick2, BLOCK_ANGLES + 1);
		float lastLastLastYaw = g_alReplayData.Get(tick3, BLOCK_ANGLES + 1);
		float avg2 = lastLastYaw - (GCNormaliseYaw(lastLastYaw - lastLastLastYaw) * 0.5);
		
		newYaw = avg1 - (GCNormaliseYaw(avg1 - avg2) * 0.5);
	}
	
	float newAngles[3];
	newAngles[0] = replayTick.angles[0];
	newAngles[1] = newYaw;
	
	TeleportEntity(client, replayTick.origin, newAngles, replayTick.velocity);
	SetEntityFlags(client, replayTick.flags);
	SetEntityMoveType(client, replayTick.moveType);
	GCSetClientDuckSpeed(client, g_alReplayData.Get(playbackTick, BLOCK_DUCKSPEED));
	GCSetClientDuckAmount(client, g_alReplayData.Get(playbackTick, BLOCK_DUCKAMOUNT));
	
	playbackTick++;
}

void ReadyReplayBot()
{
	if (botIndex && !IsValidEntity(botIndex))
	{
		// Try to respawn the bot once.
		if (!RespawnBot())
		{
			LogError("[stas] ERROR: Replay bot doesn't exist");
			return;
		}
	}
	
	// set bot name
	char formattedTime[16];
	GCFormatTickTimeHHMMSS(g_alReplayData.Length, 1.0 / GetTickInterval(), formattedTime, sizeof formattedTime);
	
	char botName[64];
	FormatEx(botName, sizeof botName, "[STAS] %s", formattedTime);
	SetClientName(botIndex, botName);
	
	ReplayTick replayTick;
	g_alReplayData.GetArray(0, replayTick);
	
	// set bot starting values
	SetEntityFlags(botIndex, replayTick.flags);
	FakeClientCommand(botIndex, "use weapon_hkp2000"); // ghetto way of switching to usp
	TeleportEntity(botIndex, replayTick.origin, replayTick.angles, NULL_VECTOR);
	SetEntityMoveType(botIndex, MOVETYPE_NONE);
}

bool RespawnBot()
{
	if (botIndex && IsValidEntity(botIndex))
	{
		AcceptEntityInput(botIndex, "kill");
	}
	// BUG: TODO: sometimes respawning the bot doesn't work at all. Maybe try respawning the bot with requestframe
	botIndex = CreateFakeClient(DEFAULT_BOT_NAME);
	if (botIndex)
	{
		DispatchKeyValue(botIndex, "classname", "fakeclient");
		DispatchSpawn(botIndex);
		ActivateEntity(botIndex);
		CS_SwitchTeam(botIndex, CS_TEAM_CT);
		CS_UpdateClientModel(botIndex);
		CS_RespawnPlayer(botIndex);
		// ghetto way of switching to usp
		FakeClientCommand(botIndex, "use weapon_hkp2000");
	}
	
	return botIndex != 0;
}

public Action Timer_StartPlayback(Handle timer)
{
	playbackTick = 0;
}

public Action Timer_StopPlayback(Handle timer)
{
	if (!IsValidEntity(botIndex) || !IsFakeClient(botIndex))
	{
		return;
	}
	playbackTick = -1;
	SetClientName(botIndex, DEFAULT_BOT_NAME);
	g_alReplayData.Clear();
}

// stolen/borrowed from danzay http://bitbucket.org/kztimerglobalteam/gokz/
public Action Timer_SpectateBot(Handle timer, int data)
{
	int client = GetClientOfUserId(data);
	
	if (GCIsValidClient(client) && GCIsValidClient(botIndex))
	{
		SetEntProp(client, Prop_Send, "m_iObserverMode", 4);
		SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", botIndex);
	}
	return Plugin_Continue;
}

// path - path the replay was saved to
// replayName - replay name to save to. leave blank for automatic naming
// nameMaxlen - max length of the replay name
bool SaveTAS(int client, char path[PLATFORM_MAX_PATH] = "", char replayName[REPLAY_NAME_LENGTH] = "")
{
	char mapName[128];
	GetCurrentMap(mapName, sizeof mapName);
	GetMapDisplayName(mapName, mapName, sizeof mapName);
	int arrayLength = g_alRecording[client].Length;
	// without the file extension
	
	if (replayName[0] == '\0')
	{
		Format(replayName, sizeof replayName, "%s_%i", mapName, arrayLength);
	}
	
	// TODO: what the hell is this BuildPath mess?
	BuildPath(Path_SM, path, sizeof path, "%s/%s", REPLAY_PATH_SM, mapName);
	if (!DirExists(path))
	{
		BuildPath(Path_SM, path, sizeof path, REPLAY_PATH_SM);
		if (!DirExists(path))
		{
			CreateDirectory(path, 511);
		}
		
		BuildPath(Path_SM, path, sizeof path, "%s/%s", REPLAY_PATH_SM, mapName);
		if (!DirExists(path))
		{
			CreateDirectory(path, 511);
		}
	}
	
	BuildPath(Path_SM, path, sizeof path, "%s/%s/%s%s", REPLAY_PATH_SM, mapName, replayName, REPLAY_SUFFIX);
	
	// handle exact same replay names
	for (int i; FileExists(path); i++)
	{
		BuildPath(Path_SM, path, sizeof path, "%s/%s/%s_%i%s", REPLAY_PATH_SM, mapName, replayName, i, REPLAY_SUFFIX);
	}
	
	File file = OpenFile(path, "wb");
	if (file == null)
	{
		CPrintToChat(client, "%s Failed to create replay file to write to: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
		return false;
	}
	
	// Prepare more data
	char steamID2[24];
	GetClientAuthId(client, AuthId_Steam2, steamID2, sizeof steamID2);
	
	char playerName[32];
	GetClientName(client, playerName, sizeof playerName);
	
	// TODO: probably should check if writes returned an error
	// Write header
	file.WriteInt32(REPLAY_MAGIC_NUMBER);
	file.WriteInt8(REPLAY_FMT_VERSION);
	file.WriteInt32(view_as<int>(1.0 / GetTickInterval())); // tickrate.
	file.WriteInt32(GetTime()); // unix 32 bit time
	file.WriteInt8(strlen(steamID2));
	file.WriteString(steamID2, false);
	file.WriteInt8(strlen(playerName));
	file.WriteString(playerName, false);
	file.WriteInt32(strlen(mapName));
	file.WriteString(mapName, false);
	file.WriteInt32(arrayLength);
	
	// Write tick data
	for (int tickIndex = 0; tickIndex < arrayLength; tickIndex++)
	{
		any replayTick[BLOCKSIZE];
		any lastReplayTick[BLOCKSIZE];
		
		g_alRecording[client].GetArray(tickIndex, replayTick);
		if (tickIndex > 0)
		{
			g_alRecording[client].GetArray(tickIndex - 1, lastReplayTick);
		}
		else if (tickIndex == 0)
		{
			// write first tick
			replayTick[BLOCK_REPLAYFLAGS] = (1 << (BLOCKSIZE - 1)) - 1; // first - 1 is for not tracking replayFlags. second is to get bitmask for available replayflags
			file.Write(replayTick, sizeof replayTick, 4);
			continue;
		}
		
		// generate replayflags
		int replayFlags;
		for (int i = 1; i < sizeof replayTick; i++)
		{
			if (replayTick[i] ^ lastReplayTick[i])
			{
				replayFlags |= 1 << (i - 1);
			}
		}
		
		// write blocks
		file.WriteInt32(replayFlags);
		for (int i = 1; i < sizeof replayTick; i++)
		{
			if (replayFlags & (1 << (i - 1)))
			{
				file.WriteInt32(replayTick[i]);
			}
		}
	}
	delete file;
	return true;
}

bool LoadTAS(int client, char[] filename, ArrayList &replayData)
{
	char mapName[128];
	GetCurrentMap(mapName, sizeof mapName);
	GetMapDisplayName(mapName, mapName, sizeof mapName);
	
	char path[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, path, sizeof path, "%s/%s/%s%s", REPLAY_PATH_SM, mapName, filename, REPLAY_SUFFIX);
	
	File file = OpenFile(path, "rb");
	if (file == null)
	{
		CPrintToChat(client, "%s Failed to open replay file for reading: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
		return false;
	}
	
	// Read header
	int magicNumber;
	file.ReadInt32(magicNumber);
	
	if (magicNumber != REPLAY_MAGIC_NUMBER)
	{
		CPrintToChat(client, "%s Failed to load replay, magic number didn't match: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
		delete file;
		return false;
	}
	
	int formatVersion;
	file.ReadInt8(formatVersion);
	
	// 100 is the earliest version
	// make sure version is between 100 and current version (inclusive)
	if (REPLAY_EARLIEST_VERSION > formatVersion > REPLAY_FMT_VERSION)
	{
		CPrintToChat(client, "%s Failed to load replay file with unsupported format version: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
		delete file;
		return false;
	}
	
	float fReplayTickrate;
	file.ReadInt32(view_as<int>(fReplayTickrate));
	
	if (fReplayTickrate != (1.0 / GetTickInterval()))
	{
		CPrintToChat(client, "%s Replay tickrate (%.2f) doesn't match the server's tickrate (%.2f): {olive}\"%s\"{default}.",
			CHAT_PREFIX, fReplayTickrate, 1.0 / GetTickInterval(), path);
	}
	
	// unused
	int unixTime;
	file.ReadInt32(unixTime);
	
	int strLen;
	
	// unused
	file.ReadInt8(strLen);
	char steamID2[24];
	file.ReadString(steamID2, sizeof steamID2, strLen);
	
	file.ReadInt8(strLen);
	char playerName[32];
	file.ReadString(playerName, sizeof playerName, strLen);
	
	// map name
	file.ReadInt32(strLen);
	char replayMapName[128];
	file.ReadString(replayMapName, sizeof replayMapName, strLen);
	
	if (!StrEqual(mapName, replayMapName))
	{
		CPrintToChat(client, "%s Failed to load replay: Replay map {olive}\"%s\"{default} doesn't match the current map {olive}\"%s\"{default}: {olive}\"%s\"{default}.",
			replayMapName, mapName, CHAT_PREFIX, path);
		delete file;
		return false;
	}
	
	int iArrayLength;
	file.ReadInt32(iArrayLength);
	
	if (replayData == null)
	{
		replayData = new ArrayList(BLOCKSIZE);
	}
	else
	{
		replayData.Clear();
	}
	
	// Read tick data
	any replayTick[BLOCKSIZE];
	for (int tickIndex = 0; tickIndex < iArrayLength; tickIndex++)
	{
		// NOTE: we don't care about saving replayflags.
		// they're just for reading/writing for now.
		int replayFlags;
		file.ReadInt32(replayFlags);
		for (int i = 0; i < sizeof replayTick; i++)
		{
			if (!(replayFlags & (1 << i)))
			{
				continue;
			}
			file.ReadInt32(replayTick[i + 1]);
		}
		
		// WRITE TO ARRAY
		replayData.PushArray(replayTick, sizeof replayTick);
	}
	delete file;
	return true;
}